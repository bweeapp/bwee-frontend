(function () {
  'use strict';

  angular
    .module('BweeFrontend.services')
    .factory('AuthorizationService', AuthorizationService);

  function AuthorizationService($auth, $state, APP){
    var service = {
      requireLogin: requireLogin,
      welcomeIfUserLoggedIn: welcomeIfUserLoggedIn
    };
    return service;

    function requireLogin(){
      return $auth.validateUser().then(function loggedIn(user){
          return user;
        }, function notLoggedIn(){
          return $state.go(APP.loginState);
        });
    }

    function welcomeIfUserLoggedIn(){
      return $auth.validateUser().then(function loggedIn(){
        return $state.go(APP.afterLoginState);
      }, function notLoggedIn(){
        return true;
      });
    }
  }
})();
