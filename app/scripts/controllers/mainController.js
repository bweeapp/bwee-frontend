'use strict';

/**
 * @ngdoc function
 * @name BweeFrontend.controller:MainController
 * @description
 * # MainController
 */
angular.module('BweeFrontend')
  .controller('MainController', function(user, $scope) {
    $scope.currentUser = user;
  });
