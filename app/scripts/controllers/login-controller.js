(function () {
  'use strict';

  angular
    .module('BweeFrontend')
    .controller('LoginController', LoginController);

  function LoginController($auth,
                           $state,
                           $rootScope,
                           APP){
    var loginVM = this;
    loginVM.logout = logout;
    loginVM.authenticate = authenticate;

    $rootScope.$on('auth:login-success', function(){
      $state.go(APP.afterLoginState);
    });

    function logout(){
      return $auth.signOut().then(function(){
        return $state.go(APP.loginState);
      });
    }

    function authenticate(){
      return $auth.authenticate('facebook');
    }
  }
})();
