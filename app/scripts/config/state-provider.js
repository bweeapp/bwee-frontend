(function () {
  'use strict';

  angular
    .module('BweeFrontend.config')
    .config(configStateProvider);

  function configStateProvider($stateProvider, $urlRouterProvider){
    // register $http interceptors, if any. e.g.
    // $httpProvider.interceptors.push('interceptor-name');

    // Application routing
    $stateProvider
      .state('login', {
        url: '/login',
        controller: 'LoginController',
        controllerAs: 'loginVM',
        templateUrl: 'templates/login/index.html',
        resolve: {
          auth: function (AuthorizationService){
            return AuthorizationService.welcomeIfUserLoggedIn();
          }
        }
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/app/main.html',
        controller: 'MainController',
        resolve: {
          user: function (AuthorizationService){
            return AuthorizationService.requireLogin();
          }
        }
      })
      .state('app.welcome', {
        url: '/welcome',
        cache: true,
        views: {
          'viewContent': {
            templateUrl: 'templates/welcome/index.html',
            controller: 'WelcomeController'
          }
        }
      })
      .state('app.home', {
        url: '/not-done-yet',
        cache: true,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/home.html',
            controller: 'HomeController'
          }
        }
      })
      .state('app.settings', {
        url: '/settings',
        cache: true,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/settings.html',
            controller: 'SettingsController'
          }
        }
      });


    // redirects to default route for undefined routes
    $urlRouterProvider.otherwise('/login');
  }
})();
