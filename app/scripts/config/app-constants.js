(function () {
  'use strict';

  angular
    .module('BweeFrontend.config')
    .constant('APP', {
      loginState: 'login',
      afterLoginState: 'app.welcome'
    });
})();
