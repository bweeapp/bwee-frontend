(function () {
  'use strict';

  angular.module('BweeFrontend.config')
  .config(function ($authProvider, bweeENV) {
    console.log(bweeENV);
    $authProvider.configure({
      apiUrl: bweeENV.apiHost + '/api',
      emailSignInPath: '/auth/user/sign_in',
      signOutUrl: '/auth/sign_out',
      tokenValidationPath: '/auth/validate_token',
      storage: 'localStorage',
      emailRegistrationPath: '/auth/user',
      accountUpdatePath:  '/users/account',
      passwordResetPath: '/auth/user/password',
      passwordUpdatePath: '/auth/user/password',
      omniauthWindowType: 'newWindow',
      authProviderPaths: {
        facebook: '/auth/facebook'
      },
      passwordResetSuccessUrl: window.location.origin
    });
  });
})();
