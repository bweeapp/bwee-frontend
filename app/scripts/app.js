'use strict';

/**
 * @ngdoc overview
 * @name BweeFrontend
 * @description
 * # Initializes main application and routing
 *
 * Main module of the application.
 */


angular.module('BweeFrontend', [
  'ionic',
  'ngCordova',
  'ngResource',
  'ngConstants',
  'BweeFrontend.fake-storage',
  'ng-token-auth',
  'BweeFrontend.config',
  'BweeFrontend.services',
  'BweeFrontend.controllers'
])
  .run(function($ionicPlatform) {

    $ionicPlatform.ready(function() {
      // save to use plugins here
    });

    // add possible global event handlers here

  });

angular.module('BweeFrontend.config', []);
angular.module('BweeFrontend.services', []);
angular.module('BweeFrontend.controllers', []);
angular.module('BweeFrontend.fake-storage', []);
