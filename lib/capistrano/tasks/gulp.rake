namespace :gulp do
  task :release do
    Dir.chdir fetch(:rsync_stage) do
      run_locally do
        bower_components_src = File.expand_path(
          '../../../bower_components/',
          File.dirname(__FILE__)
        )
        bower_components_target = File.expand_path(
          '../../../tmp/deploy/bower_components',
          File.dirname(__FILE__)
        )
        execute :ln, "-fs #{bower_components_src} #{bower_components_target}"
        execute "gulp", "--build --environment #{fetch(:stage)}" or raise
      end
    end
  end
end
