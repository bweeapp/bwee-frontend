namespace :bwee do
  task :mock_cordova do
    on roles(:app) do
      within release_path do
        execute :touch, 'release/www/cordova.js'
      end
    end
  end

  task :copy_configs do
    Dir.chdir fetch(:rsync_stage) do
      run_locally do
        config_src = "config"
        config_target = "release/"
        execute :cp, "-r #{config_src} #{config_target}"
      end
    end
  end
end
